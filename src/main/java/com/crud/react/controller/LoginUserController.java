package com.crud.react.controller;

import com.crud.react.DTO.LoginDto;
import com.crud.react.DTO.UserDto;
import com.crud.react.model.LoginUser;
import com.crud.react.response.CommonResponse;
import com.crud.react.response.ResponseHelper;
import com.crud.react.service.LoginUserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/user")
public class LoginUserController {

    @Autowired
    private LoginUserService loginUserService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/all")
    public CommonResponse<List<LoginUser>> getAllUser() {
        return ResponseHelper.ok(loginUserService.getAllUser());
    }

    @GetMapping("/{id}")
    public CommonResponse <LoginUser> getUserById(@PathVariable("id")Long id) {
        return ResponseHelper.ok(loginUserService.getUserById(id)) ;
    }

//    @PostMapping
//    public CommonResponse<LoginUser> addUser(@RequestBody LoginUser loginUser) {
//        return ResponseHelper.ok(loginUserService.addUser(loginUser));
//    }

    @PostMapping(consumes = "multipart/form-data" )
    public CommonResponse<LoginUser> addUser(UserDto userDto, @RequestPart("file") MultipartFile multipartFile ) {
        return ResponseHelper.ok(loginUserService.addUser(modelMapper.map(userDto,LoginUser.class),multipartFile));
    }

    @PostMapping ("/sign-in")
    public CommonResponse<Map<String, Object>> login(@RequestBody LoginDto loginDto) {
        return ResponseHelper.ok(loginUserService.login(loginDto));
    }

    @PutMapping(path = "/{id}",consumes = "multipart/form-data" )
    public CommonResponse<LoginUser> editUserById(@PathVariable("id") Long id,UserDto userDto,@RequestPart("file") MultipartFile multipartFile ) {
        return ResponseHelper.ok(loginUserService.editUserById(id,modelMapper.map(userDto, UserDto.class),multipartFile));
    }

    @DeleteMapping("/{id}")
    public void deleteUSerById(@PathVariable("id") Long id) { loginUserService.deleteUserById(id);}
}
