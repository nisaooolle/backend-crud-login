package com.crud.react.controller;

import com.crud.react.DTO.ProductDTO;
import com.crud.react.model.Product;
import com.crud.react.response.CommonResponse;
import com.crud.react.response.ResponseHelper;
import com.crud.react.service.ProductService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    ModelMapper modelMapper;

    @GetMapping("/all")
    public CommonResponse<Page<Product>> getAllProduct(@RequestParam(required = false) int page, @RequestParam(required = false)String nama) {
        return ResponseHelper.ok(productService.getAllProduct(page , nama==null?"" :nama));
    }

    @GetMapping("/{id}")
    public CommonResponse<Product> getProductById(@PathVariable("id")Long id) {
        return ResponseHelper.ok(productService.getProductById(id)) ;
    }

    @PostMapping(consumes = "multipart/form-data")
    public CommonResponse<Product> addProduct(ProductDTO productDTO, @RequestPart("file")MultipartFile multipartFile) {
        return ResponseHelper.ok(productService.addProduct(productDTO, multipartFile));
    }

    @PutMapping(path = "/{id}", consumes = "multipart/form-data")
    public CommonResponse<Product> editProductById(@PathVariable("id") Long id,ProductDTO productDTO, @RequestPart("file") MultipartFile multipartFile) {
        return ResponseHelper.ok(productService.editProductById(id,productDTO, multipartFile));
    }

    @DeleteMapping("/{id}")
    public void deleteProductById(@PathVariable("id") Long id) { productService.deleteProductById(id);}

}
