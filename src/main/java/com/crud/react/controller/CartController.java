package com.crud.react.controller;

import com.crud.react.DTO.CartDTO;
import com.crud.react.model.Cart;
import com.crud.react.response.CommonResponse;
import com.crud.react.response.ResponseHelper;
import com.crud.react.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/cart")
public class CartController {
    @Autowired
    private CartService cartService;

    @PostMapping
    public CommonResponse<Cart>create(@RequestBody CartDTO cart) {
        return ResponseHelper.ok(cartService.create(cart));
    }

    @GetMapping("/all")
    public CommonResponse<Page<Cart>> findAll(@RequestParam int page, @RequestParam Long login_user_id) {
        return ResponseHelper.ok(cartService.findAll(page, login_user_id));
    }

    @PutMapping("/{id}")
    public CommonResponse<Cart> update(@PathVariable("id") long id, @RequestBody CartDTO cart) {
        return ResponseHelper.ok(cartService.update(id, cart));
    }

    @DeleteMapping("/{id}")
    public CommonResponse<Map<String, Object>> delete(@PathVariable("id") long id) {
        return ResponseHelper.ok(cartService.delete(id));
    }

    @DeleteMapping("/checkout")
    public CommonResponse<Map<String,Boolean>> checkout(@RequestParam Long loginUserId ) {
        return ResponseHelper.ok(cartService.checkout(loginUserId));
    }
}
