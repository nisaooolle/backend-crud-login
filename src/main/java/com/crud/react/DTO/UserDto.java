package com.crud.react.DTO;

public class UserDto {
    private String email;
    private String password;
    private String role;

    private String nama;

    private String alamat;

    private Integer noTlpn;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public Integer getNoTlpn() {
        return noTlpn;
    }

    public void setNoTlpn(Integer noTlpn) {
        this.noTlpn = noTlpn;
    }
}
