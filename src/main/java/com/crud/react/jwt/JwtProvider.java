package com.crud.react.jwt;

import com.crud.react.Exception.InternalErrorException;
import com.crud.react.Exception.NotFoundException;
import com.crud.react.model.LoginUser;
import com.crud.react.model.TemporaryToken;
import com.crud.react.repository.LoginUserRepository;
import com.crud.react.repository.TemporaryTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

@Component
public class JwtProvider {

    private static String seccetKey = "belajar Spring";

    private static Integer expired = 900000;

    @Autowired
    private TemporaryTokenRepository temporaryTokenRepository;
    @Autowired
    private LoginUserRepository loginUserRepository;

//    public String generateToken(UserDetails userDetails) {
//        return Jwts.builder()
//                .setSubject(userDetails.getUsername()) // subject username
//                .setIssuedAt(new Date()) // waktu
//                .setExpiration(new Date(new Date().getTime()+expired)) // menambahkan waktu
//                .signWith(SignatureAlgorithm.HS512, seccetKey) // digunakan untuk menambhkan kata token menggunakan
//                .compact(); // akan mmebuat token dan membuat data token itu ke string
//    }

    public String generateToken(UserDetails userDetails) {
        String token = UUID.randomUUID().toString().replace("_","");
        LoginUser loginUser = loginUserRepository.findByEmail(userDetails.getUsername()).orElseThrow(() -> new NotFoundException("User not found generate token"));
        var chekingToken = temporaryTokenRepository.findByUserId(loginUser.getId());
        if (chekingToken.isPresent())  temporaryTokenRepository.deleteById(chekingToken.get().getId());
        TemporaryToken temporaryToken = new TemporaryToken();
        temporaryToken.setToken(token);
        temporaryToken.setExpiredDate(new Date(new Date().getTime() + expired));
        temporaryToken.setUserId(loginUser.getId());
        temporaryTokenRepository.save(temporaryToken);
        return token;
    }

    public TemporaryToken getSubject(String token) {
        return temporaryTokenRepository.findByToken(token).orElseThrow(() -> new InternalErrorException("Token error parse"));
    }

    public  boolean checkingTokenJwt(String token) {
        System.out.println(token);
        TemporaryToken tokenExist = temporaryTokenRepository.findByToken(token).orElse(null);
        if (tokenExist == null ) {
            System.out.println("Token kosong");
            return  false;
        }

        if (tokenExist.getExpiredDate().before(new Date())) {
            System.out.println("Token Expired");
            return false;
        }
        return true;
    }

//    public String getSubject(String token) {
//        return Jwts.parser().setSigningKey(seccetKey).parseClaimsJws(token).getBody().getSubject();
//        //sudah bawaan ,mengambil data seccetkey, mencocokkan isi token
//    }


//    public boolean checkingTokenJwt(String token) {
//        try {
//            Jwts.parser().setSigningKey(seccetKey).parseClaimsJws(token);
//            return true;
//        }catch (SignatureException e) {
//            System.out.println("invalid JWT signature -> Message: {} ");
//        }catch (MalformedJwtException e) {
//            System.out.println("invalid JWT token -> Message: {} ");
//        }catch(ExpiredJwtException e) {
//            System.out.println("Expired JWT token -> Message: {} ");
//        }catch (UnsupportedJwtException e) {
//            System.out.println("Unsuported JWT token -> message : {} ");
//        }catch (IllegalArgumentException e) {
//            System.out.println("JWT claims string is emmpty -> message: {}");
//        }
//        return false;
//    }


}
