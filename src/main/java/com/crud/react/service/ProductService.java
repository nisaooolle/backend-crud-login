package com.crud.react.service;

import com.crud.react.DTO.ProductDTO;
import com.crud.react.model.LoginUser;
import com.crud.react.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ProductService {
    Product addProduct (ProductDTO productDTO, MultipartFile multipartFile);
    Product getProductById (Long id);

    Page<Product> getAllProduct(int page,String nama);

    Product editProductById(Long id ,ProductDTO productDTO,MultipartFile multipartFile);

    void deleteProductById(Long Id);

}
