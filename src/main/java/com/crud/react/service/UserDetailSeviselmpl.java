package com.crud.react.service;

import com.crud.react.model.LoginUser;
import com.crud.react.model.UserPriciple;
import com.crud.react.repository.LoginUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailSeviselmpl implements UserDetailsService {

    @Autowired
    private LoginUserRepository loginUserRepository; // memanggil repository

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        LoginUser loginUser = loginUserRepository.findByEmail(username).orElseThrow(() -> new UsernameNotFoundException("username not found"));
        return UserPriciple.build(loginUser);
    }
}
