package com.crud.react.service;

import com.crud.react.DTO.LoginDto;
import com.crud.react.DTO.UserDto;
import com.crud.react.model.LoginUser;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface LoginUserService {

    Map<String, Object> login(LoginDto loginDto);
    LoginUser addUser (LoginUser loginUser, MultipartFile multipartFile);

    LoginUser getUserById (Long id);

    List<LoginUser> getAllUser();

    LoginUser editUserById(Long id, UserDto userDto, MultipartFile multipartFile);

    Map<String,Boolean> deleteUserById(Long Id);
}
