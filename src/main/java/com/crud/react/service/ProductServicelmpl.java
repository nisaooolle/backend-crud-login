package com.crud.react.service;

import com.crud.react.DTO.ProductDTO;
import com.crud.react.Exception.InternalErrorException;
import com.crud.react.Exception.NotFoundException;
import com.crud.react.model.Product;
import com.crud.react.repository.ProductRepository;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;

@Service
public class ProductServicelmpl implements ProductService{
    @Autowired
    ProductRepository productRepository;

    private static final String DOWNLOAD_URL = "https://firebasestorage.googleapis.com/v0/b/react-crud-658dd.appspot.com/o/%s?alt=media";

    @Override
    public Product addProduct(ProductDTO productDTO, MultipartFile multipartFile) {
        String url = imageConverter(multipartFile);
        Product products = new Product(url, productDTO.getNama(), productDTO.getDeskripsi(),productDTO.getHarga());
        return productRepository.save(products);
    }

    private String imageConverter(MultipartFile multipartFile) {
        try {
            String fileName = getExtentions(multipartFile.getOriginalFilename());
            File file = convertToFile(multipartFile, fileName);
            var RESPONSE_URL = uploadFile(file,fileName);
            file.delete();
            return RESPONSE_URL;
        }catch (Exception e) {
            e.getStackTrace();
            throw new InternalErrorException("Error upload file");
        }
    }

    private  File convertToFile(MultipartFile multipartFile,String fileName) throws IOException {
        File file = new File(fileName);
        try(FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(multipartFile.getBytes());
            fos.close();
        }
        return file;
    }

    private String uploadFile(File file, String fileName) throws IOException {
        BlobId blobId = BlobId.of("react-crud-658dd.appspot.com", fileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("media").build();
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream("./src/main/resources/serviceAccountKey.json"));
        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
        storage.create(blobInfo, Files.readAllBytes(file.toPath()));
        return String.format(DOWNLOAD_URL, URLEncoder.encode(fileName, StandardCharsets.UTF_8));
    }

    private String getExtentions(String fileName) {
        return fileName.split("\\.")[0];
    }

    @Override
    public Product getProductById(Long id) {
        return productRepository.findById(id).orElseThrow(() -> new NotFoundException("Id tidak ditemukan"));
    }

    @Override
    public Page<Product> getAllProduct(int page, String nama) {
        Pageable pages = PageRequest.of(page,5);
            return productRepository.findByNama(nama,pages);
    }

    @Override
    public Product editProductById(Long id,ProductDTO productDTO, MultipartFile multipartFile) {
        String url = imageConverter(multipartFile);
       Product product = productRepository.findById(id).get();
        product.setImage(url);
        product.setHarga(productDTO.getHarga());
        product.setNama(productDTO.getNama());
        product.setDeskripsi(productDTO.getDeskripsi());
       return productRepository.save(product);
    }

    @Override
    public void deleteProductById(Long Id) {
        productRepository.deleteById(Id);
    }

}
