package com.crud.react.service;

import com.crud.react.DTO.CartDTO;
import com.crud.react.Exception.NotFoundException;
import com.crud.react.model.Cart;
import com.crud.react.model.Product;
import com.crud.react.repository.CartRepository;
import com.crud.react.repository.LoginUserRepository;
import com.crud.react.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CartServicelmpl implements CartService{

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private LoginUserRepository loginUserRepository;

    @Autowired
    private ProductRepository productRepository;
    @Override
    public Cart create(CartDTO cart) {
        Product product = productRepository.findById(cart.getProductId()).orElseThrow(() -> new NotFoundException("Product id tidak ditemukan"));
        Cart create = new Cart();
        create.setQuantity(cart.getQuantity());
        create.setTotalHarga(product.getHarga() * cart.getQuantity());
        create.setLoginUserId(loginUserRepository.findById(cart.getLoginUserId()).orElseThrow(() -> new NotFoundException("User id tidak di temukan")));
        create.setProductId(product);
        return cartRepository.save(create);
    }

    @Override
    public Page<Cart> findAll(int page,Long login_user_id) {
        Pageable pageable = PageRequest.of(page, 5);
        return cartRepository.findByLoginUser(login_user_id,pageable);
    }

    @Override
    public Cart update(long id, CartDTO cart) {
        Cart data = cartRepository.findById(id).orElseThrow(() -> new NotFoundException("Id not Found"));
        data.setQuantity(cart.getQuantity());
        data.setLoginUserId(loginUserRepository.findById(cart.getLoginUserId()).orElseThrow(() -> new NotFoundException("User id tidak ditemukan")));
        data.setProductId(productRepository.findById(cart.getProductId()).orElseThrow(() -> new NotFoundException("Product id tidak t")));
        return cartRepository.save(data);
    }

    @Override
    public Map<String, Object> delete(long id) {
        cartRepository.deleteById(id);
        Map<String,Object> obj =new HashMap<>();
        obj.put("DELETE", true);
        return obj;
    }

    @Override
    public Map<String, Boolean> checkout(Long loginUserId) {
        List<Cart> cartList = cartRepository.checkout(loginUserId);
        cartRepository.deleteAll(cartList);
        Map<String, Boolean> obj = new HashMap<>();
        obj.put("deleted", Boolean.TRUE);
        return obj;
    }

}

