package com.crud.react.service;

import com.crud.react.DTO.CartDTO;
import com.crud.react.model.Cart;
import org.springframework.data.domain.Page;

import java.util.Map;

public interface CartService {
    Cart create(CartDTO cart);

    Page<Cart> findAll(int page, Long login_user_id);

    Cart update(long id, CartDTO cart);

    Map<String,Object> delete(long id);

    Map<String,Boolean> checkout (Long loginUserId);
}
