package com.crud.react.service;

import com.crud.react.DTO.LoginDto;
import com.crud.react.DTO.UserDto;
import com.crud.react.Exception.InternalErrorException;
import com.crud.react.Exception.NotFoundException;
import com.crud.react.enumeted.UserType;
import com.crud.react.jwt.JwtProvider;
import com.crud.react.model.LoginUser;
import com.crud.react.repository.LoginUserRepository;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class LoginUserServicelmpl implements LoginUserService{

    @Autowired
    LoginUserRepository loginUserRepository;

    @Autowired
    UserDetailsService userDetailsService;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    PasswordEncoder passwordEncoder;

    private static final String DOWNLOAD_URL = "https://firebasestorage.googleapis.com/v0/b/react-crud-658dd.appspot.com/o/%s?alt=media";

    @Override
    public LoginUser addUser(LoginUser loginUser, MultipartFile multipartFile) {
        String url = imageConverter(multipartFile);
        String email = loginUser.getEmail();
        loginUser.setPassword(passwordEncoder.encode(loginUser.getPassword()));
        if (loginUser.getRole().name().equals("ADMIN"))
            loginUser.setRole(UserType.ADMIN);
        else loginUser.setRole(UserType.USER);

        var validasi = loginUserRepository.findByEmail(email);
        if (validasi.isPresent()) {
            throw new InternalErrorException("Maaf Email sudah digunakan");
        }
        loginUser.getNama();
        loginUser.getAlamat();
        loginUser.getNoTlpn();
        loginUser.setFotoProfile(url);
        return loginUserRepository.save(loginUser);
    }

    private String imageConverter(MultipartFile multipartFile) {
        try {
            String fileName = getExtentions(multipartFile.getOriginalFilename());
            File file = convertToFile(multipartFile, fileName);
            var RESPONSE_URL = uploadFile(file,fileName);
            file.delete();
            return RESPONSE_URL;
        }catch (Exception e) {
            e.getStackTrace();
            throw new InternalErrorException("Error upload file");
        }
    }

    private  File convertToFile(MultipartFile multipartFile,String fileName) throws IOException {
        File file = new File(fileName);
        try(FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(multipartFile.getBytes());
            fos.close();
        }
        return file;
    }

    private String uploadFile(File file, String fileName) throws IOException {
        BlobId blobId = BlobId.of("react-crud-658dd.appspot.com", fileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("media").build();
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream("./src/main/resources/serviceAccountKey.json"));
        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
        storage.create(blobInfo, Files.readAllBytes(file.toPath()));
        return String.format(DOWNLOAD_URL, URLEncoder.encode(fileName, StandardCharsets.UTF_8));
    }

    private String getExtentions(String fileName) {
        return fileName.split("\\.")[0];
    }

    @Override
    public LoginUser getUserById(Long id) {
        return loginUserRepository.findById(id).orElseThrow(() -> new NotFoundException("Id tidak ditemukan"));
    }

    @Override
    public List<LoginUser> getAllUser() {
            return loginUserRepository.findAll();
    }


    private String authories(String email, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        }catch (BadCredentialsException e) {
            throw new InternalErrorException("Email Or Password Not found");
        }
        UserDetails userDetails = userDetailsService.loadUserByUsername(email);
        return jwtProvider.generateToken(userDetails);
    }

    @Override
    public Map<String, Object> login(LoginDto loginDto) {
        String token = authories(loginDto.getEmail(),loginDto.getPassword());
        LoginUser loginUser = loginUserRepository.findByEmail(loginDto.getEmail()).get();
        Map<String, Object> response = new HashMap<>();
        response.put("token" , token);
        response.put("expired", "15 menit");
        response.put("user" , loginUser);
        return response;
    }
    @Transactional
    @Override
    public LoginUser editUserById(Long id, UserDto userDto, MultipartFile multipartFile) {
        String url = imageConverter(multipartFile);
        userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));
        LoginUser update = loginUserRepository.findById(id).orElseThrow(() -> new NotFoundException("Tidak Ada"));
        var validasi = loginUserRepository.findByEmail(userDto.getEmail());
        if (validasi.isPresent()) {
            throw new InternalErrorException("Maaf email sudah ada");
        }
        update.setEmail(userDto.getEmail());
        update.setPassword(userDto.getPassword());
        update.setNama(userDto.getNama());
        update.setAlamat(userDto.getAlamat());
        update.setNoTlpn(userDto.getNoTlpn());
        update.setFotoProfile(url);
        return update;

    }
    @Override
    public Map<String, Boolean> deleteUserById(Long id) {
        try {
            loginUserRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }

}
