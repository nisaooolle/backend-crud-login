package com.crud.react.Exception;

public class EmailException extends RuntimeException{
    public EmailException(String message) {
        super(message);
    }

}
