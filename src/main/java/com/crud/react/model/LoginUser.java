package com.crud.react.model;

import com.crud.react.enumeted.UserType;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class LoginUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column (name = "email")
    private String email;

    @Column (name = "password")
    private  String password;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "role")
    private UserType role;

    @Column(name = "nama")
    private String nama;

    @Column(name = "alamat")
    private String alamat;

    @Lob
    @Column(name = "foto_profile")
    private String fotoProfile;

    @Column(name = "no_tlpn")
    private Integer noTlpn;

//    @Enumerated(EnumType.STRING)
//    @Column(name = "user_type")
//    private UserType userType;

    public LoginUser() {
    }

    public LoginUser(String email, String password, UserType role, String nama, String alamat, String fotoProfile, Integer noTlpn) {
        this.email = email;
        this.password = password;
        this.role = role;
        this.nama = nama;
        this.alamat = alamat;
        this.fotoProfile = fotoProfile;
        this.noTlpn = noTlpn;
    }
    //    public LoginUser(Long id, String email, String password) {
//        this.id = id;
//        this.email = email;
//        this.password = password;
//    }


    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getFotoProfile() {
        return fotoProfile;
    }

    public void setFotoProfile(String fotoProfile) {
        this.fotoProfile = fotoProfile;
    }

    public Integer getNoTlpn() {
        return noTlpn;
    }

    public void setNoTlpn(Integer noTlpn) {
        this.noTlpn = noTlpn;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

//    public UserType getUserType() {
//        return userType;
//    }
//
//    public void setUserType(UserType userType) {
//        this.userType = userType;
//    }

    public UserType getRole() {
        return role;
    }

    public void setRole(UserType role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "LoginUser{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
