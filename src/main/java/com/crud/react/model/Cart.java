package com.crud.react.model;

import javax.persistence.*;
import java.util.Optional;

@Entity
@Table(name = "cart")
public class Cart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "total_harga")
    private float totalHarga;
    @Column(name = "quantity")
    private int quantity;
    @ManyToOne
    @JoinColumn(name = "login_user_id")
        private LoginUser loginUserId;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product productId;

    public Cart() {
    }

    public Cart(Long id, LoginUser loginUserId, Product productId) {
        this.id = id;
        this.loginUserId = loginUserId;
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    public void setTotalHarga(float totalHarga) {
        this.totalHarga = totalHarga;
    }

    public float getTotalHarga() {
        return totalHarga;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LoginUser getLoginUserId() {
        return loginUserId;
    }

    public void setLoginUserId(LoginUser loginUserId) {
        this.loginUserId = loginUserId;
    }

    public Product getProductId() {
        return productId;
    }

    public void setProductId(Product productId) {
        this.productId = productId;
    }
}
