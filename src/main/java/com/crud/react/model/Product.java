package com.crud.react.model;

import javax.persistence.*;

@Entity
@Table(name = "product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column (name = "nama")
    private String nama;

    @Lob
    @Column(name = "deskripsi")
    private String deskripsi;

    @Lob
    @Column(name = "image")
    private String image;

    @Column (name = "harga")
    private float harga;

    public Product(String image, String nama, String deskripsi, float harga) {
        this.image = image;
        this.nama = nama;
        this.deskripsi = deskripsi;
        this.harga = harga;
    }

    public Product() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public float getHarga() {
        return harga;
    }

    public void setHarga(float harga) {
        this.harga = harga;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", nama='" + nama + '\'' +
                ", deskripsi='" + deskripsi + '\'' +
                ", image='" + image + '\'' +
                ", harga=" + harga +
                '}';
    }
}
