package com.crud.react.repository;

import com.crud.react.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface ProductRepository extends JpaRepository<Product,Long> {
    @Query(value = "select * from product where nama LIKE CONCAT ('%',:nama, '%')", nativeQuery = true)
    Page<Product> findByNama(String nama, Pageable pageable );

}
