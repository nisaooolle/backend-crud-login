package com.crud.react.repository;

import com.crud.react.model.Cart;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CartRepository extends JpaRepository<Cart,Long> {
    @Query(value = "select * from cart where login_user_id = :login_user_id ", nativeQuery = true)
    Page<Cart> findByLoginUser(Long login_user_id, Pageable pageable );

    @Query(value = "select * from cart where login_user_id = :login_user_id ", nativeQuery = true)
    List<Cart> checkout(Long login_user_id);

}
